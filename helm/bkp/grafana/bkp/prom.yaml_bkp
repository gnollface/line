{{- $root := . -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-prom
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-prom
  replicas: {{ .Values.replicaCount }} # tells deployment to run ${replicaCount} pods matching the template
  strategy:
    rollingUpdate:
      maxSurge: {{ .Values.maxSurge }} # tells how many more replicase can be created above replicas level while updating. (4*50%=2+4=6max) Can be integer
      maxUnavailable: {{ .Values.maxUnavailable }} #tells how many unavailable pods can be while updating. Can be percentage.
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-prom
    spec:
      volumes:
      - name: {{ .Values.volumes.volumeMounts.volname }}
        persistentVolumeClaim:
          claimName: {{ .Values.volumes.name }}-claim #nginx-pv-claim
#      - name: config
#        configMap:
#          name: {{ .Values.configmap.name  }}
      containers:
      - image: {{ .Values.container.image }} # prom/prometheus:latest
        name: {{ .Release.Name }}-prom
        envFrom:
        - configMapRef:
            name: {{ .Values.configmap.name }} # configmap-prom
        ports:
        - containerPort: {{ .Values.port }} #80
        resources:
          requests:
            cpu: {{ .Values.resources.requests.cpu }} #10m
            memory: {{ .Values.resources.requests.mem }} #100Mi
          limits:
            cpu: {{ .Values.resources.limits.cpu }} #100m
            memory: {{ .Values.resources.limits.mem }} #100Mi
        tty: true
        stdin: true
        volumeMounts:
          - name: {{ .Values.volumes.volumeMounts.volname }}
            mountPath: {{ .Values.volumes.volumeMounts.configMount }}
            subPath:
#          - name: config
#            mountPath: {{ .Values.prom.config }}
#            subPath: prometheus.yml
